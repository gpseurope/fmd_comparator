# copyright (2022) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
# Licence:XXXXXXXXXXXX
# Authors:
#   Arthur Fontaine
#   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>

import os, sys, getopt, T2Comparator, sqlalchemy, config, Url, Url_Colletion

VERSION = "1.2.1"


def checkT2(node, dgw_station_list):
    #### CHECK T2 (File's metadata) ####
    print("(T2) File's metadata : ")
    print(" Getting station's list...")
    t2_comparator = T2Comparator.T2Comparator(node, dgw_station_list)

    print(" Looking for differences...")
    t2_comparator.search_differences()

    if t2_comparator.error_dict["nb_differences"] == 0:
        print(" No differences found !")
    else:
        print(
            " "
            + str(t2_comparator.error_dict["nb_differences"])
            + " Differences found !"
        )


def list_nodes_names():
    nodes = []
    to_avoid = ["DGW", "PGW"]
    import Url, json
    from urllib.request import urlopen

    with urlopen(
        "https://gnssdata-epos.oca.eu/GlassFramework/webresources/t0-manager/getNodes"
    ) as response:  # get the json files
        result = json.load(response)
        for i in range(len(result)):
            if result[i]["NodeName"] not in to_avoid:
                nodes.append(result[i]["NodeName"])
    return nodes


if __name__ == "__main__":
    # create an empty output folder in the file ridectory if it doesn't exist
    if not os.path.exists(os.path.join(os.path.dirname(__file__), "output")):
        os.makedirs(os.path.join(os.path.dirname(__file__), "output"))

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-n", help="the name of the node")
    parser.add_argument("-l", action="store_true", help="list all the nodes names")
    parser.add_argument("-v", action="store_true", help="show the version")
    args = parser.parse_args()
    node = ""
    do_all = False
    if not args.n and not args.l and not args.v:
        print(parser.print_help())
        sys.exit()
    elif args.v:
        print("Version : " + VERSION)
        sys.exit()

    elif args.l:
        for n in list_nodes_names():
            print(n)
    else:
        node = args.n
        print("Compare DGW / " + node + " : ")
        # Stations list
        print("Request station list from database...")
        engine = sqlalchemy.create_engine(
            config.BBD_DIALECT
            + "://"
            + config.BDD_USERNAME
            + ":"
            + config.BDD_PSWD
            + "@"
            + config.BDD_HOST
            + ":"
            + config.BDD_PORT
            + "/"
            + config.BDD_DATABASE,
            echo=False,
        )
        local_station_list = {}
        with engine.connect() as con:
            # dgw_table = con.execute("SELECT s.marker FROM connections c JOIN node n ON n.id = c.destiny JOIN station s ON s.id = c.station WHERE n.name = '"+node+"';")
            local_table = con.execute(
                "SELECT s.marker Marker, net.name Network FROM station s JOIN station_network sn ON sn.id_station = s.id JOIN network net ON net.id = sn.id_network;"
            )
            for val in local_table:
                if val[0] not in local_station_list:
                    local_station_list.update({val[0]: [val[1]]})
                else:
                    local_station_list[val[0]].append(val[1])
        node_url = Url.Url(node)
        checkT2(node_url, local_station_list)
