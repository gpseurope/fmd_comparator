# copyright (2022) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
# Licence:XXXXXXXXXXXX
# Authors:
#   Arthur Fontaine
#   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>

from urllib.request import urlopen
from urllib.error import URLError
import json, Output, Url

JSON_SIMPLE_ITEMS = [
    'md5_checksum, "creation_date',
    "file_size",
    "md5_uncompressed",
    "revision_date",
    "name",
    "reference_date",
    "relative_path",
    "published_date",
    "station_marker",
    "status",
]
NODE_DATACENTER = {
    "IPGP": "IPGP",
    "Italian National Node for RING": "RING",
    "NOA": "NOA",
    "CzechGeo": "GOP",
    "EPOS-GNSS Pan-European Node": "EPOSGNSS",
    "ROB-EUREF": "EPN_HDC",
    "Portuguese EPOS National Node": "C4G",
    "French-Node": "RENAG",
    "Romanian National Node": "ROGNSS",
    "Spanish EPOS Data node": "IGE",
    "PGW": "",
}


class T2Comparator:
    def __init__(self, node_url: Url, stations_list: dict):
        self.url = node_url
        self.node = node_url.name
        self.output = Output.Output(self.node, "T2")
        self.list_station = stations_list
        self.list_station = sorted(
            sorted(self.list_station.items()), key=lambda k: k[1][0]
        )
        self.error_dict = {"nb_differences": 0}

    def search_differences(self):
        for station in self.list_station:
            # if station[0] in ['SOPH', 'AUTN', 'MARS', 'NICE', 'LILL', 'WLBH']:
            try:
                # get the json files
                with urlopen(
                    self.url.node
                    + self.url.T2_request
                    + "marker="
                    + station[0]
                    + "/json"
                ) as response:
                    node_json = json.load(response)
            except URLError:
                print("Error ! Problem with json download on :", self.node)
                self.output.send_error_mail(
                    "Error request",
                    "Coudn't download Local Node JSON file from the API \nIP = "
                    + self.url.node,
                )
                return

            try:
                with urlopen(
                    self.url.dgw
                    + self.url.T2_request
                    + "marker="
                    + station[0]
                    + "/json"
                ) as response:
                    dgw_json = json.load(response)
            except URLError:
                print("Error ! Problem with json download on : DGW")
                self.output.send_error_mail(
                    "Error request",
                    "Coudn't download Local Node JSON file from the API \n IP = "
                    + self.url.dgw,
                )
                return

            self.error_dict.update(
                {
                    station[0]: {
                        "nb_error": 0,
                        "nb_status": {
                            "node_-3": 0,
                            "node_-2": 0,
                            "node_-1": 0,
                            "node_0": 0,
                            "node_1": 0,
                            "node_2": 0,
                            "node_3": 0,
                            "node_>_0": 0,
                            "dgw_<_0": 0,
                            "dgw_=_0": 0,
                            "dgw_>_0": 0,
                        },
                    }
                }
            )
            self.output.write(
                "=================    "
                + station[0]
                + " "
                + str(station[1])
                + "    =================\n"
            )

            # Check station's files status in node
            node_status_checked = []  # List of files where status is > 0
            for file in range(len(node_json)):
                if node_json[file]["status"] > 0:
                    if node_json[file]["status"] == 1:
                        self.error_dict[station[0]]["nb_status"]["node_1"] += 1
                        self.error_dict[station[0]]["nb_status"]["node_>_0"] += 1
                    elif node_json[file]["status"] == 2:
                        self.error_dict[station[0]]["nb_status"]["node_2"] += 1
                        self.error_dict[station[0]]["nb_status"]["node_>_0"] += 1
                    if node_json[file]["status"] == 3:
                        self.error_dict[station[0]]["nb_status"]["node_3"] += 1
                        self.error_dict[station[0]]["nb_status"]["node_>_0"] += 1

                    node_status_checked.append(node_json[file])
                if node_json[file]["status"] == 0:
                    self.error_dict[station[0]]["nb_status"]["node_0"] += 1
                if node_json[file]["status"] == -1:
                    self.error_dict[station[0]]["nb_status"]["node_-1"] += 1
                if node_json[file]["status"] == -2:
                    self.error_dict[station[0]]["nb_status"]["node_-2"] += 1
                if node_json[file]["status"] == -3:
                    self.error_dict[station[0]]["nb_status"]["node_-3"] += 1

            # Check station's files status in dgw
            dgw_datacenter_checked = (
                []
            )  # List of files where data center == node data center
            for id in range(len(dgw_json)):
                if dgw_json[id]["data_center"]["acronym"] in NODE_DATACENTER[self.node]:
                    dgw_datacenter_checked.append(dgw_json[id])
                    if dgw_json[id]["status"] < 0:
                        self.error_dict[station[0]]["nb_status"]["dgw_<_0"] += 1
                    if dgw_json[id]["status"] == 0:
                        self.error_dict[station[0]]["nb_status"]["dgw_=_0"] += 1
                    if dgw_json[id]["status"] > 0:
                        self.error_dict[station[0]]["nb_status"]["dgw_>_0"] += 1

            # Fill table with status files <= 0 at DGW
            self.output.shouldNotBeInDGW += (
                self.error_dict[station[0]]["nb_status"]["dgw_<_0"]
                + self.error_dict[station[0]]["nb_status"]["dgw_=_0"]
            )

            # write stats in result file
            self.output.write_stats_T2(self.error_dict[station[0]])

            # check files's infos with status > 0
            if len(node_status_checked) == len(dgw_datacenter_checked):
                for file_nb in range(len(node_status_checked)):
                    node_file = node_status_checked[file_nb]
                    dgw_file = self.find_file(dgw_datacenter_checked, node_file["name"])
                    if dgw_file != 0:
                        for key in node_file:
                            if key in JSON_SIMPLE_ITEMS:
                                self.compare(
                                    station[0],
                                    "Name : ["
                                    + node_file["name"]
                                    + "] md5 local node : ["
                                    + node_file["md5_checksum"]
                                    + "] error : ["
                                    + key
                                    + "]",
                                    dgw_file[key],
                                    node_file[key],
                                )

                            if key == "file_type":
                                self.compare(
                                    station[0],
                                    "Name : ["
                                    + node_file["name"]
                                    + "] md5 local node : ["
                                    + node_file["md5_checksum"]
                                    + "] error : ["
                                    + key
                                    + " => format]",
                                    dgw_file[key]["format"],
                                    node_file[key]["format"],
                                )
                                self.compare(
                                    station[0],
                                    "Name : ["
                                    + node_file["name"]
                                    + "] md5 local node : ["
                                    + node_file["md5_checksum"]
                                    + "] error : ["
                                    + key
                                    + " => sampling_window]",
                                    dgw_file[key]["sampling_window"],
                                    node_file[key]["sampling_window"],
                                )
                                self.compare(
                                    station[0],
                                    "Name : ["
                                    + node_file["name"]
                                    + "] md5 local node : ["
                                    + node_file["md5_checksum"]
                                    + "] error : ["
                                    + key
                                    + " => sampling_frequency]",
                                    dgw_file[key]["sampling_frequency"],
                                    node_file[key]["sampling_frequency"],
                                )

                            if key == "data_center":
                                self.compare(
                                    station[0],
                                    "Name : ["
                                    + node_file["name"]
                                    + "] md5 local node : ["
                                    + node_file["md5_checksum"]
                                    + "] error : ["
                                    + key
                                    + " => hostname]",
                                    dgw_file[key]["hostname"],
                                    node_file[key]["hostname"],
                                )
                                self.compare(
                                    station[0],
                                    "Name : ["
                                    + node_file["name"]
                                    + "] md5 local node : ["
                                    + node_file["md5_checksum"]
                                    + "] error : ["
                                    + key
                                    + " => protocol]",
                                    dgw_file[key]["protocol"],
                                    node_file[key]["protocol"],
                                )
                                self.compare(
                                    station[0],
                                    "Name : ["
                                    + node_file["name"]
                                    + "] md5 local node : ["
                                    + node_file["md5_checksum"]
                                    + "] error : ["
                                    + key
                                    + " => acronym]",
                                    dgw_file[key]["acronym"],
                                    node_file[key]["acronym"],
                                )
                                self.compare(
                                    station[0],
                                    "Name : ["
                                    + node_file["name"]
                                    + "] md5 local node : ["
                                    + node_file["md5_checksum"]
                                    + "] error : ["
                                    + key
                                    + " => data_center_structure => directory_naming]",
                                    dgw_file[key]["data_center_structure"][
                                        "directory_naming"
                                    ],
                                    node_file[key]["data_center_structure"][
                                        "directory_naming"
                                    ],
                                )
                    else:
                        self.error_dict[station[0]]["nb_error"] += 1
                        self.error_dict["nb_differences"] += 1
                        self.output.write(
                            "File not found on DGW | name : ["
                            + node_file["name"]
                            + "] (Node)"
                        )
            else:
                self.output.write(
                    "Missing file(s) | "
                    + str(self.error_dict[station[0]]["nb_status"]["dgw_>_0"])
                    + " (DGW) / "
                    + str(self.error_dict[station[0]]["nb_status"]["node_>_0"])
                    + " (Node)"
                )
                self.error_dict[station[0]]["nb_error"] += 1
                self.error_dict["nb_differences"] += 1
            self.output.write("\n")
        self.output.send_mail(self.error_dict)

    def find_file(self, dgw_list, name):
        for file in range(len(dgw_list)):
            if dgw_list[file]["name"] == name:
                return dgw_list[file]
        return 0

    def compare(self, station, txt, dgw_val, local_val):
        if local_val != dgw_val:
            self.error_dict["nb_differences"] += 1
            self.error_dict[station]["nb_error"] += 1
            self.output.write(
                txt + " | " + str(dgw_val) + " (DGW) / " + str(local_val) + " (Node)\n"
            )
