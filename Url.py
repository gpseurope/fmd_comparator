# copyright (2022) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
# Licence:XXXXXXXXXXXX
# Authors:
#   Arthur Fontaine
#   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>

from urllib.request import urlopen
import json


class Url:
    def __init__(self, *args):
        self.name = args[0]
        self.dgw = "https://gnssdata-epos.oca.eu/"
        if len(args) == 1:
            self.node = self.getUrl(args[0])
            self.T2_request = "GlassFramework/webresources/files/combination/"
        elif len(args) == 2:
            self.node = self.format_url(args[1])
            self.T2_request = "GlassFramework/webresources/files/combination/"

    def getUrl(self, node: str) -> str:
        with urlopen(
            self.dgw + "GlassFramework/webresources/t0-manager/getNodes"
        ) as response:  # get the json files
            result = json.load(response)
        for i in range(len(result)):
            if result[i]["NodeName"] == node:
                return self.format_url(result[i]["NodeUrl"])

    def format_url(self, url: str):
        res = url.split("GlassFramework")[0]
        if res[len(res) - 1] != "/":
            res += "/"
        return res
