from datetime import date
from os import stat
import smtplib, config
from urllib import error
from os.path import dirname, join, basename
from email.message import EmailMessage
from prettytable import PrettyTable


class Output:
    def __init__(self, node: str, metadata_type: str):
        self.node = node
        self.type = metadata_type
        self.names = {"T1": "DB_Check_Station_Metadata", "T2": "DB_Check_File_Metadata"}
        self.path_file = (
            dirname(__file__)
            + "/output/"
            + self.names[self.type]
            + "_"
            + node.replace(" ", "_")
            + "_"
            + date.today().strftime("%d-%m-%Y")
            + ".txt"
        )
        self.file = open(self.path_file, "w")
        self.path_stat_file = (
            dirname(__file__)
            + "/output/"
            + self.names[self.type]
            + "_"
            + node.replace(" ", "_")
            + "_"
            + date.today().strftime("%d-%m-%Y")
            + "_stat.txt"
        )
        self.stat_file = open(self.path_stat_file, "w")
        self.dgw_missing_files_number = 0
        self.local_node_missing_files_number = 0

        if metadata_type == "T2":
            self.shouldNotBeInDGW = 0

    def write(self, string: str):
        self.file.write(string)

    def write_stats_T1(
        self,
        nb_error: int,
        miss_dgw: list,
        miss_node: list,
        rinex_0: list,
        __station_error: dict,
    ):
        
        self.stat_file.write(self.node + "\n\n")
        self.stat_file.write("Missing station(s) on dgw : " + str(miss_dgw) + "\n")
        self.stat_file.write("Missing station(s) on node : " + str(miss_node) + "\n")
        # self.stat_file.write("Station without rinex files : " + str(rinex_0) + '\n\n')
        self.stat_file.write("\nTotal errors \n" + str(nb_error) + " error(s) \n")
        table = PrettyTable()
        table.field_names = [
            "Station",
            "logs",
            "station_items",
            "station_networks",
            "station_contacts",
            "location",
            "geological",
            "others",
            "Total",
        ]
        for station in __station_error:
            table.add_row(
                [
                    station["marker"],
                    station["nb_log_error"],
                    station["nb_item_error"],
                    station["nb_network_error"],
                    station["nb_contact_error"],
                    station["nb_loc_error"],
                    station["nb_geological_error"],
                    station["nb_simple_error"],
                    int(station["nb_error"]),
                ]
            )
        self.stat_file.write(str(table))

    def write_stats_T2(self, __station_error: dict):
        values = __station_error["nb_status"]
        if (
            values["dgw_<_0"] != 0
            or (values["node_-3"] + values["node_-2"] + values["node_-1"]) != 0
        ):
            self.file.write(
                "Status < 0 | "
                + str(values["dgw_<_0"])
                + "(DGW) / "
                + str(values["node_-3"] + values["node_-2"] + values["node_-1"])
                + " (Node)\n"
            )
        if values["dgw_=_0"] != 0 or values["node_0"] != 0:
            self.file.write(
                "Status = 0 | "
                + str(values["dgw_=_0"])
                + "(DGW) / "
                + str(values["node_0"])
                + " (Node)\n"
            )
        self.file.write(
            "Status > 0 | "
            + str(values["dgw_>_0"])
            + "(DGW) / "
            + str(values["node_1"] + values["node_2"] + values["node_3"])
            + " (Node)\n"
        )
        self.file.write("\n")

    def send_mail(self, error_dict: dict):
        self.file.close()
        self.stat_file.close()
        message = EmailMessage()
        message["From"] = config.SENDER_MAIL
        message["To"] = config.RECEIVER_MAIL
        if self.type == "T1":
            message["Subject"] = config.SUBJECT_T1.replace("{NODE}", self.node)
            message.set_content(open(self.path_stat_file, "r").read())
            message.add_attachment(
                open(
                    join(dirname(__file__), "./doc/readme_station_metadata.html"), "r"
                ).read(),
                filename="readme_station_metadata.html",
            )
        else:
            message["Subject"] = config.SUBJECT_T2.replace("{NODE}", self.node)
            message.set_content(self.create_mail_content(error_dict))
            message.add_attachment(
                open(
                    join(dirname(__file__), "./doc/readme_file_metadata.html"), "r"
                ).read(),
                filename="readme_file_metadata.html",
            )
        message.add_attachment(
            open(self.path_file, "r").read(), filename=basename(self.path_file)
        )
        server = smtplib.SMTP(config.MAIL_SERVER_ADDRESS, config.MAIL_SERVER_PORT)
        server.sendmail(config.SENDER_MAIL, config.RECEIVER_MAIL, message.as_string())
        server.quit()

    def create_mail_content(self, error_dict: dict):
        table = PrettyTable()
        table.field_names = [
            "Station",
            "3",
            "2",
            "1",
            "0",
            "-1",
            "-2",
            "-3",
            "Total",
            "Total > 0",
            "DGW",
            "Diff",
            "Errors",
        ]
        
        for station in error_dict:
            if station != "nb_differences":
                values = error_dict[station]["nb_status"]
                table.add_row(
                    [
                        station,
                        self.format_result(values["node_3"]),
                        self.format_result(values["node_2"]),
                        self.format_result(values["node_1"]),
                        self.format_result(values["node_0"]),
                        self.format_result(values["node_-1"]),
                        self.format_result(values["node_-2"]),
                        self.format_result(values["node_-3"]),
                        self.format_result(
                            values["node_3"]
                            + values["node_2"]
                            + values["node_1"]
                            + values["node_0"]
                            + values["node_-1"]
                            + values["node_-2"]
                            + values["node_-3"]
                        ),
                        self.format_result(
                            values["node_3"] + values["node_2"] + values["node_1"]
                        ),
                        self.format_result(
                            values["dgw_<_0"] + values["dgw_=_0"] + values["dgw_>_0"]
                        ),
                        self.checkdiff(
                            (values["node_3"] + values["node_2"] + values["node_1"]),
                            (values["dgw_<_0"] + values["dgw_=_0"] + values["dgw_>_0"]),
                        ),
                        self.format_result(error_dict[station]["nb_error"]),
                    ]
                )
        return (
            """
Node
"""
            + self.node
            + """

Status <= 0 at the DGW
"""
            + str(self.shouldNotBeInDGW)
            + """

Number of missing files at the DGW = 
"""
            + str(self.dgw_missing_files_number)
            + """

Number of missing files at the local node = 
"""
            + str(self.local_node_missing_files_number)
            + """

# Total number of differences in the metadata for files present at the DGW and local node
"""
            + str(error_dict["nb_differences"])
            + """
 
"""
            + str(table)
        )

    def send_error_mail(self, error_type: str, error_message: str):
        message = EmailMessage()
        message["From"] = config.SENDER_MAIL
        message["To"] = config.RECEIVER_MAIL
        if self.type == "T1":
            message["Subject"] = (
                config.SUBJECT_T1.replace("{NODE}", self.node) + " : " + error_type
            )
        else:
            message["Subject"] = (
                config.SUBJECT_T2.replace("{NODE}", self.node) + " : " + error_type
            )
        message.set_content(error_message)
        server = smtplib.SMTP(config.MAIL_SERVER_ADDRESS, config.MAIL_SERVER_PORT)
        server.sendmail(config.SENDER_MAIL, config.RECEIVER_MAIL, message.as_string())
        server.quit()

    def checkdiff(self, node: int, dgw: int):
        if (node - dgw) < 0:
            self.local_node_missing_files_number += (node - dgw)
            return "<"
        elif (node - dgw) == 0:
            return ""
        elif (node - dgw) > 0:
            self.dgw_missing_files_number +=  (node - dgw)
            return ">"

    def format_result(self, value: int):
        if value == 0:
            return "-"
        return value
