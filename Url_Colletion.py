# copyright (2022) CNRS UMR GEOAZUR OBSERVATOIRE DE LA COTE D'AZUR
# Licence:XXXXXXXXXXXX
# Authors:
#   Arthur Fontaine
#   Jean-Luc Menut <jean-luc.menut@geaozur.unice.fr>

import Url, json
from urllib.request import urlopen

class Url_Collection:
    def __init__(self):
        self.list: list[Url.Url] = []
        self.to_avoid = ["DGW"] #DGW to avoid comparing DGW with itself
    def getAllNodes(self):
        with urlopen("https://gnssdata-epos.oca.eu/GlassFramework/webresources/t0-manager/getNodes") as response: # get the json files
            result = json.load(response)
        for i in range(len(result)):
            if result[i]["NodeName"] not in self.to_avoid:
                self.list.append(Url.Url(result[i]["NodeName"], result[i]["NodeUrl"]))