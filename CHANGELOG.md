# 2023-04-11
Update to version 1.2
Better messages

# 2022-06-28
Update to version 1.1
The mail server is configurable in the config.py file.

# 2022-06-27
Update to version 1.0.1
Add the option to display the version of the software
Add the creation of the output folder