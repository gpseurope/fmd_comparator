# Introduction

This file describes the structure of the email and of the attached file that are the result of the comparison of file metadata between the Data Gateway (DGW) and a Local node.

The metadata compared are the JSON output of the EPOS_GLASS_Framework API. There are collected with the API call:
`https://\<url>:\<port>/GlassFramework/webresources/files/combination/marker=\<MARKER>/json` 


To really understand the reported errors, it is necessary to know the structure of a station metadata JSON file. To get the files metadata in the JSON format for a single station, use the command line client:
```
./pyglass.py -u http://<url>:8080/GlassFramework files -m <MARKER> > <MARKER>.json
```
Where `url` is the url of the API and `MARKER` the 4 characters marker of the station.
Example:

```
./pyglass.py -u http://gnssdata-epos.oca.eu:8080/GlassFramework files\
 -m <MARKER> > <MARKER>.json
```

## **Email description**

This part describes the structure of the email. This is a summary of the discrepancies found between the DGW and the local node.

```
Node :
<Name of the node>

Status <= 0 at the DGW
<Number of files with status <= 0 at the dgw>

Number of missing files at the DGW = <Number of files that are at the local node and  should be also at the DGW >

Number of missing files at the local node = <Number of files that are at the DGW but not at the local node>

Total number of differences in the metadata for files present at the DGW and local node(s)
<total number of differences between node files and dgw files>

`| Station  |   3   |   2  |    1   |   0  |   -1  |   -2  |   -3   |    total     |`
<Marker>    |  <Number of files with each file status at the node>  |<From -3 to 3>|
`total > 0   |      DGW        |            diff           |        # errors       |`
<From 1 to 3>|<nb files at DGW>|<between total>0 and DGW>0>|<total number of error>|


```

## **Attachment description**
This part describes the structure of the attachement. The file list all the stations by alphabetic order for which discrepancies were found in the file metadata. It is structured as the following:

```
======= Station Name [Network(s)] ========

Status < 0 | <value> (DGW) / <value> (Node)
Status = 0 | <value> (DGW) / <value> (Node)
Status > 0 | <value> (DGW) / <value> (Node)

Missing file(s) | <value> (DGW) / <value> (Node)

Name : [<filename>] md5 local node : [<md5checksum>] error : [<key>] | <value> (DGW) / <value> (Node)

File not found on DGW | name : [<filename>] (Node)'
```

The lines starting with `Status` show the number of files with a status < 0 (resp. = 0 and > 0) on the DGW and the local node for the stations/

The line stating with `missing files` shows when there are files with status > 0 on a node that are missing on the other node. This is the same as the `status >0` line and is used to enhance the visibility of this case. Moreover, if this line appears the comparison stops here.

The lines starting with `Name:` show the file present on both the DGW and the local node, but for which the metadata are different.
The `<filename>` refers to the name of the file (`"name"` in the JSON), the `<md5checksum>` is the same as in the JSON, and help to find the file concerned by the error. Then, `<key>` is the element in the json object, where the error is located. Finally, the first `<value>` is from the DGW, and the second one from the local Node.

The line starting with `File not found on DGW` | shows when the number of files with status > 0 is the same on both nodes but not all `<filename>` are the same on both nodes.

### Examples : 
In all these examples, there are 3 files for the station ALPE with a status < 0 at the local node, and 0 at the DGW.

**If there is one (or more) file(s) with status > 0 missing at the DGW this errors shows up:**
```
=================    ALPE ['RENAG']    =================
Status < 0 | 0(DGW) / 3 (Node)
Status = 0 | 0(DGW) / 0 (Node)
Status > 0 | 4438(DGW) / 4443 (Node)

Missing file(s) | 4438 (DGW) / 4443 (Node)
```
With the information given by the line `Status > 0`, we can see that `5` files are missing on the DGW that are present on local node, that means 5 files have not been synchronized.

**If a file couldn't be found on DGW:**
```
=================    ALPE ['RENAG']    =================
Status < 0 | 0(DGW) / 3 (Node)
Status = 0 | 0(DGW) / 0 (Node)
Status > 0 | 4438(DGW) / 4438 (Node)

File not found on DGW | name : [alpe0780.20d.Z] (Node)'
```
The number of files is the same on both sides (regarding the `Status > 0`), but the file `alpe0780.20d.Z` wasn't found on the DGW. And one not displayed for the moment, wasn't found on the local node.

**If everything is fine but one or more difference(s) shows up in the metadata:**
```
=================    ALPE ['RENAG']    =================
Status < 0 | 0(DGW) / 3 (Node)
Status = 0 | 0(DGW) / 0 (Node)
Status > 0 | 4438(DGW) / 4438 (Node)

name : alpe0780.20d.Z md5 local node : 7554bd4b39d65af1ab9371f3108c112d status | 2 (DGW) / 1 (Node)
```
Here: for one of the file present in both nodes, there was an error in the metadata detected: the file os named `alpe0780.20d.Z`, it has a md5checksum of `7554bd4b39d65af1ab9371f3108c112d` (on the local node), and the error is on its `status`. On the DGW the `status` is `2` and on the local Node it's `1`.
