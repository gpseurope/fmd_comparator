# File Metadata Comparator

**Author**: Arthur Fontaine, Jean-Luc Menut (menut@geoazur.unice.fr)

**version**: 1.2
## Description 

This software compares the file metadata provided by the EPOS GLASS Framework API at the local node and at the DGW, and sends the result of the comparison to a specified email address. 

The email body contains a summary of the result and two files are attached: one is the detailled version of the result and the other an html file that explains how to read the summary and detailled result.

To interpret the results, it is necessary to know the structure of the file metadata JSON. You can use the command line client to download an example of the file metadata.

## Issues and comments:

If you find issues or have some comments, please add them to the gitlab issuetracker https://gitlab.com/gpseurope/issuetracker with the label `file metadata comparator`.

If you need more information on how to use this software you can also write to gnss-dw@oca.eu

## How to configure the application

The tool is configured by copying config.py.template to config.py and adding the needed information:

* SENDER_MAIL: the mail that is sending the resultrs of the comparison
* RECEIVER_MAIL: the mail that is receiving the resulsts of the comparison
* BDD_USERNAME: the username to connect the local node DB
* BDD_PSWD: the password to connect the local node DB
* BDD_HOST: the host of the local node DB
* BDD_PORT: the port of the local node DB 
* BDD_DATABASE: the name of the local node DB

You can put the same email address into sender and receiver if needed
or modify the subject as you want

**You will need a SMTP server running on the machine that runs this application**
### Requirements
---
* python3.8 [python website](https://www.python.org/downloads/)
* pip3
* sqlalchemy
* psycopg2-binary
* prettytable

### How to use the application: 
---
This option list the parameters of the software:

    python3 metadata_comparator.py -h 

This option list the nodes names:

    python3 metadata_comparator.py -h 

The command to compare the metadata is: 

    python3 metadata_comparator.py -n <"NodeName">

Exemple: to compare the DGW and the French node:

    python3 metadata_comparator.py -n "French-Node"
